package br.com.algartelecom

def scmVars
def cloneRepository(){
    stage('Clone repository') {
        scmVars = checkout scm
    }
}

def pullBuilder(imageName, imageVersion,dockerCredentialsId){
    stage('Pull docker builder'){
        withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: "${dockerCredentialsId}",
        usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
            sh "docker login -u ${env.USERNAME} -p ${env.PASSWORD} registry.algartelecom.com.br:5000"
            sh "docker pull registry.algartelecom.com.br:5010/${imageName}:${imageVersion}"
            sh "docker logout registry.algartelecom.com.br:5000"
        }
    }  
}

def updateBuilderImage(imageName,dockerCredentialsId){
	stage('Update docker builder'){
		
		def containerUserName = 'jenkins'
		def containerUserUUID = 497

		if(params.makeBuilder == true ){
			withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: "${dockerCredentialsId}",
            usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
				if(params.builderVersion != 'latest'){
					sh "docker login -u ${env.USERNAME} -p ${env.PASSWORD} registry.algartelecom.com.br:5000"
					sh "docker build --build-arg user=${containerUserName} --build-arg uuid=${containerUserUUID} -f build.Dockerfile -t registry.algartelecom.com.br:5000/${imageName}:${params.builderVersion} ."
					sh "docker tag registry.algartelecom.com.br:5000/${imageName}:${params.builderVersion} registry.algartelecom.com.br:5010/${imageName}:latest"
					sh "docker push registry.algartelecom.com.br:5000/${imageName}:${params.builderVersion}"
					sh "docker push registry.algartelecom.com.br:5000/${imageName}:latest"
					sh "docker logout registry.algartelecom.com.br:5000"
				}else{
					error('Please select an valid version')
				}
			}
		}
	}
}

def build(imageName, imageVersion, credentialsId){
    stage('Build'){
        try { 
   			def shortCommit = sh(returnStdout: true, script: "git log -n 1 --pretty=format:'%h'").trim()
            withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: "${credentialsId}",
            usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
                sh "git remote set-url origin https://${env.USERNAME}:${env.PASSWORD}@\$(echo ${scmVars.GIT_URL} | awk -v RS='//' '{ print \$1 }' | grep 'bit')"
                sh "git checkout ${scmVars.GIT_BRANCH}"
                sh "git fetch"
                sh "git reset --hard ${scmVars.GIT_BRANCH}"
            }
            if( currentBuild.rawBuild.getCauses()[0] =~ /BranchIndexingCause/){
                println "CAUSE: SCM"
                sh "docker run --rm -v \$(pwd):/c -v /var/lib/jenkins/.dockerm2:/m registry.algartelecom.com.br:5010/${imageName}:${imageVersion} mvn clean install deploy -DbuildNumber=hash${shortCommit}b${env.BUILD_ID} -DskipTests -nsu"
            }else{
                println "CAUSE: USER"
                if(params.build == true){
                    sh "docker run --rm -v \$(pwd):/c -v /var/lib/jenkins/.dockerm2:/m registry.algartelecom.com.br:5010/${imageName}:${imageVersion} mvn clean install deploy -DbuildNumber=hash${shortCommit}b${env.BUILD_ID} -DskipTests -nsu"
                }
            }
        } catch (e) {
            throw e
        }
    }
}


@NonCPS
def groups(USER_ID) {
    return Jenkins.instance.securityRealm.loadUserByUsername(USER_ID).authorities.collect{ it.toString() };
}

def permissionCheck(allowedGroups){
	def userGroups = groups(currentBuild.rawBuild.getCause(Cause.UserIdCause).getUserId())
	allowedGroups.each{
		if( userGroups.contains(it) ){
			return true
		}
	}
	return false
}