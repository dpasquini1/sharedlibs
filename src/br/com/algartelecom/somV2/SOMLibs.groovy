package br.com.algartelecom.somV2

import br.com.algartelecom.base

def updateVersion(){
    stage('POM Version Update'){
        try {                                           
            if(params.build == true){
                if( params.release == true ){
                    if( params.module == 'all' ){
                        sh "export GRADLE_POMPATH=\$(pwd)/rpm-pack/som-service/pom.xml && export GRADLE_POMVERSION=${params.version} && /usr/local/gradle/gradle-3.2.1/bin/gradle -b support-tools/build.gradle updatePom"
                        sh "export GRADLE_POMPATH=\$(pwd)/rpm-pack/som-view/pom.xml && export GRADLE_POMVERSION=${params.version} && /usr/local/gradle/gradle-3.2.1/bin/gradle -b support-tools/build.gradle updatePom"                        
                    }
                    else{
                        sh "export GRADLE_POMPATH=\$(pwd)/rpm-pack/${params.module}/pom.xml && export GRADLE_POMVERSION=${params.version} && /usr/local/gradle/gradle-3.2.1/bin/gradle -b support-tools/build.gradle updatePom"
                    }
                }
                else{
                    if( params.module == 'all' ){
                        sh "export GRADLE_POMPATH=\$(pwd)/rpm-pack/som-service/pom.xml && export GRADLE_POMVERSION=LATEST && /usr/local/gradle/gradle-3.2.1/bin/gradle -b support-tools/build.gradle updatePom"
                        sh "export GRADLE_POMPATH=\$(pwd)/rpm-pack/som-view/pom.xml && export GRADLE_POMVERSION=LATEST && /usr/local/gradle/gradle-3.2.1/bin/gradle -b support-tools/build.gradle updatePom"
                    }
                    else{
                        sh "export GRADLE_POMPATH=\$(pwd)/rpm-pack/${params.module}/pom.xml && export GRADLE_POMVERSION=LATEST && /usr/local/gradle/gradle-3.2.1/bin/gradle -b support-tools/build.gradle updatePom"
                    }
                }
            }
        } catch (e) {
            throw e
        }
    }    
}


def deploy(repositoryName){
    stage('Deploy'){
        def baseLib = new base()
        def allowedGroups = ['authenticated']
        def allow = false 

        if(params.deploy == true){

            if(params.ambiente == 'tit'){
                if( baseLib.permissionCheck(allowedGroups) ){
                    allow = true
                }
            }else{
                allow = true
            }
            
            if(allow){
                dir('ansible-role-deploy'){
                    checkout([
                        $class: 'GitSCM', 
                        userRemoteConfigs: [[url: 'git@bitbucket.org:planetarepos/ansible-role-deploy.git',
                        credentialsId: 'd6668342-5078-4629-9334-995bbe1c9c2e']], 
                        branches: [[name: 'master']],
                        poll: false
                    ])
                    
                    sh  "cp -R ${WORKSPACE}/ansible-role-deploy/* /var/lib/jenkins/.ansible/roles/ansible-role-deploy;"
                }
                dir('values'){
                    git(
                        url: 'git@bitbucket.org:planetarepos/ansible-values.git',
                        credentialsId: 'd6668342-5078-4629-9334-995bbe1c9c2e',
                        branch: "master"
                    )
                }

                if(params.module == 'som-service' || params.module == 'all'){
                    sh "ansible-playbook -vv /var/lib/jenkins/.ansible/roles/ansible-role-deploy/tasks/create-env.yml -i ${WORKSPACE}/values/som/environments/${params.ambiente}/hosts -e modulo=ws -e system=som -e ambiente=${params.ambiente} -e workspace=${WORKSPACE} --vault-password-file=/var/lib/jenkins/.ssh/ansible-pass.yml"
                    sh "ansible-playbook -vv /var/lib/jenkins/.ansible/roles/ansible-role-deploy/ansible-role-deploy.yml -i ${WORKSPACE}/values/som/environments/${params.ambiente}/hosts -e modulo=ws -e system=som -e ambiente=${params.ambiente} -e workspace=${WORKSPACE}"
                }
                if(params.module == 'som-view' || params.module == 'all'){
                    sh "ansible-playbook -vv /var/lib/jenkins/.ansible/roles/ansible-role-deploy/tasks/create-env.yml -i ${WORKSPACE}/values/som/environments/${params.ambiente}/hosts -e modulo=app -e system=som -e ambiente=${params.ambiente} -e workspace=${WORKSPACE} --vault-password-file=/var/lib/jenkins/.ssh/ansible-pass.yml"
                    sh "ansible-playbook -vv /var/lib/jenkins/.ansible/roles/ansible-role-deploy/ansible-role-deploy.yml -i ${WORKSPACE}/values/som/environments/${params.ambiente}/hosts -e modulo=app -e system=som -e ambiente=${params.ambiente} -e workspace=${WORKSPACE}"
                }
            }
            else{
                error("YOU SHALL NOT DEPLOY HERE")
            }
        }
    }
}

def generateRPM(imageName, imageVersion, rpmPath, rpmRepositoryHostname, repositoryRpmPath){
    stage('RPM'){
        def baseLib = new base()
        def allowedGroups = ['authenticated']
        def allow = false 

        try {
            if(params.rpm == true ){
                if( baseLib.permissionCheck(allowedGroups) ){
                    allow = true
                }
                if(allow){
                    sh "docker run --rm -v \$(pwd):/c -v /var/lib/jenkins/.dockerm2:/m registry.algartelecom.com.br:5010/${imageName}:${imageVersion} mvn clean compile -DbuildNumber=hash${shortCommit}b${env.BUILD_ID}.Final -U rpm:rpm"
                    sh "scp -i /var/lib/jenkins/.ssh/id_rsa ${rpmPath}/*.rpm jenkins@${rpmRepositoryHostname}:${repositoryRpmPath} && ssh -i /var/lib/jenkins/.ssh/id_rsa jenkins@{rpmRepositoryHostname} createrepo --checksum sha ${repositoryRpmPath}"
                }
            }   
        } catch (e) {
            throw e
        }
    }
}

def cleanDir() {
    stage('Clean dir') {
        deleteDir()
    }
}