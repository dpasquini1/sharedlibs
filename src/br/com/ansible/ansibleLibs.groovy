package br.com.ansible


def deployGateway(customer,gateway) {
    stage('GATEWAY DEPLOYMENT') {
        sh "docker exec \$(docker ps -aqf \"name=jean_ansible\") ansible-wrapper ${customer} deploy-gateway null ${gateway}"
    }
}

def notifyDatabase(gateway,databaseurl,databasename,databaseuser,databasepass,percentcompleted) {
    stage('DATABASE UPDATE') {
        sh "mysql -h ${databaseurl} -u ${databaseuser} -p${databasepass} -P 3306 -e \"UPDATE ${databasename}.CHAT_GATEWAY_JOB SET PERCENT_COMPLETED = ${percentcompleted}, UPDATED_AT = '\$(date +'%F %T')' WHERE APPNAME = '${gateway}'\" "
    }
}